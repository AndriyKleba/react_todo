import React, {Component} from 'react';

import './search-panel.css';

export default class SearchPanel extends Component {

    state = {
        term: ''
    }

    onChangeInput = (e) => {
        const term = e.target.value;
        this.setState({
            term
        })
        this.props.onChangeInput(term)
    }

    render() {
        return (
            <input type="text"
                   className="form-control search-input"
                   placeholder="type to search"
                   value={this.state.term}
                   onChange={this.onChangeInput}
            />
        );
    }
}

