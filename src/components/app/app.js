import React, {Component} from 'react';

import AppHeader from '../app-header';
import SearchPanel from '../search-panel';
import TodoList from '../todo-list';
import ItemStatusFilter from '../item-status-filter';

import './app.css'
import AddItem from "../add-item";

export default class App extends Component {

    maxId = 100;

    state = {
        todoData: [
            this.createTodoItem('Drink Coffee'),
            this.createTodoItem('Make Awesome App'),
            this.createTodoItem('Have a lunch'),
            this.createTodoItem('Play Soccer')
        ],
        term: '',
        filter: 'active'
    };

    createTodoItem(label) {
        return {
            label,
            important: false,
            done: false,
            id: this.maxId++
        }
    }

    returnNewArray(arr, id, propItem) {
        const idx = arr.findIndex(el => el.id === id)
        const oldItem = arr[idx]
        const newItem = {...oldItem, [propItem]: !oldItem[propItem]}

        return [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ]
    }


    deleteItem = (id) => {
        this.setState(({todoData}) => {
            const idx = todoData.findIndex(el => el.id === id)
            const newDeleteArray = [
                ...todoData.slice(0, idx),
                ...todoData.slice(idx + 1)
            ]
            return {
                todoData: newDeleteArray
            }
        })
    }

    addItem = (text) => {
        const newItem = this.createTodoItem(text)
        this.setState(({todoData}) => {
            const newAddArray = [
                ...todoData,
                newItem
            ];

            return {
                todoData: newAddArray
            }
        })
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.returnNewArray(todoData, id, 'important')
            }
        })
    };


    onToggleDone = (id) => {
        this.setState(({todoData}) => {
            return {
                todoData: this.returnNewArray(todoData, id, 'done')
            }
        })
    };

    search(arrItem, text) {
        if (text === 0) {
            return arrItem
        }
        return arrItem.filter(item => {
            return item.label
                .toLowerCase()
                .indexOf(text.toLowerCase()) > -1
        })
    }

    checkFilter(items, filter) {
        switch (filter) {
            case 'all':
                return items
            case 'active':
                return items.filter(item => !item.done)
            case 'done':
                return items.filter(item => item.done)
            default:
               return  items
        }
    }

    onChangeInput = (term) => {
        this.setState({term})
    }

    onFilterChange = (filter) => {
        this.setState({filter})
    }

    render() {
        const {todoData, term, filter} = this.state
        const visible = this.checkFilter(this.search(todoData, term), filter)
        const doneCounter = todoData.filter(el => el.done).length
        const todoCounter = todoData.length - doneCounter;

        return (
            <div className="todo-app">
                <AppHeader toDo={todoCounter} done={doneCounter}/>
                <div className="top-panel d-flex">
                    <SearchPanel onChangeInput={this.onChangeInput}/>
                    <ItemStatusFilter filter={filter} onFilterChange={this.onFilterChange}/>
                </div>
                <TodoList todos={visible}
                          onDeleted={this.deleteItem}
                          onToggleImportant={this.onToggleImportant}
                          onToggleDone={this.onToggleDone}
                />
                <AddItem onItemAdd={this.addItem}/>
            </div>
        )
    }
};
