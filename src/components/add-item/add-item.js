import React, {Component} from "react";

import './add-item.css';

export default class AddItem extends Component {

    state = {
        label: ''
    }


    onLabelChange = (e) => {
        this.setState({
            label: e.target.value
        })
    }

    onSubmit = (e) => {
        e.preventDefault()
        this.props.onItemAdd(this.state.label)
        this.setState({
            label: ''
        })
    }

    render() {
        return (
            <form className="add_item" onSubmit={this.onSubmit}>
                <input type="text" className="form-control input-push" onChange={this.onLabelChange}
                       placeholder="I do.." value={this.state.label}/>
                <button type="button" className="btn btn-success btn-push">AddItem
                </button>
            </form>
        )
    }
}

